﻿using System;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;

namespace WebCrawler
{
    class Program
    {
        static async Task Main(string[] args)
        {
            #region Crawler
            StreamWriter sw = new StreamWriter("D:\\電機電子公會.txt", true);

            char[] Alphanumeric = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
            HttpClient client;
            HttpResponseMessage response;
            string contentstring;
            string requestUri;
            string serial;
            var config = Configuration.Default;
            var context = BrowsingContext.New(config);
            IDocument document;

            foreach (var alpha1 in Alphanumeric)
            {
                foreach (var alpha2 in Alphanumeric)
                {
                    for (int i = 0; i < 10000; i++)
                    {

                        try
                        {

                            serial = alpha1.ToString()+alpha2.ToString() + i.ToString().PadLeft(4, '0');
                            requestUri = string.Format(@"http://www.teemab2b.com.tw/NewCompanyDetail.aspx?companyAccount={0}", serial);
                            client = new HttpClient();
                            response = await client.GetAsync(requestUri);
                            contentstring = await response.Content.ReadAsStringAsync();


                            //台灣電機電子公會查無此公司網頁字串長度11324 大同股份有限公司網頁字串長度19716
                            //Console.WriteLine(contentstring.Length);


                            #region split 土炮
                            //string[] array = contentstring.Split("<");
                            //foreach (var local in array)
                            //{
                            //    if(local.Contains("lblCompanyName"))
                            //    {
                            //        string[] companyName = local.Split(">");
                            //        Console.WriteLine(companyName[1]);
                            //    }
                            //    if (local.Contains("lblEmail"))
                            //    {
                            //        string[] email = local.Split(">");
                            //        Console.WriteLine(email[1]);
                            //    }
                            //}

                            #endregion

                            #region 正規表示式

                            //string pattern = @"(?<Others>.*)";
                            //Match match = Regex.Match(contentstring, pattern);
                            //var a = match.Success;
                            //var b = match.Groups["Others"].ToString();


                            #endregion

                            #region AngleSharp
                            if (contentstring.Length < 11325)
                                continue;
                            
                            document = context.OpenAsync(res => res.Content(contentstring)).Result;

                            sw.Write(serial + "?");
                            sw.Write(document.QuerySelector("#lblCompanyName").InnerHtml + ":");
                            sw.WriteLine(document.QuerySelector("#lblEmail").InnerHtml + ",");

                            #endregion
                        }

                        catch
                        {
                            continue;
                        }

                    }
                }
            }

            sw.Close();
            #endregion




        }

        private static async Task<string> HttpResponseString(string requestUri)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(requestUri);
            string contentstring = await response.Content.ReadAsStringAsync();
            return contentstring;
        }

    }
}
